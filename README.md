# jfx-holamundo-netbeans

Aplicación JFX básica con instrucciones para configurarse y ejecutarse desde NetBeans accediendo directamente a los jar de JavaFX

## Requisitos previos
1. Tener JDK instalado y PATH apuntando a sus binarios (para que se encuentre java y javac)
2. Tener el SDK de JavaFX accesible (e.g: C:\JavaFX\javafx-sdk-11.0.2)
3. Tener Apache NetBeans

## Compilar código
1. Abrir el proyecto en NetBeans o crear uno nuevo estándar como `Java Application with Ant`
2. En propiedades del proyecto, en librerías, añadir los jar de JavaFX al classpath.
   ![librerias en el proyecto de NetBeans](img/libraries.png)
3. En propiedades del proyecto, en ejecución, añadir los siguiente parámetros a la ejecución de la máquina virtual en _VM options_:
   ```
   --module-path C:\JavaFX\javafx-sdk-11.0.2\lib --add-modules javafx.controls,javafx.fxml
   ```
   ![módulos JFX en la máquina virtual](img/vm.png)
4. Crear la clase `JavaFX.java`
5. Compilar 
6. Ejecutar
